package org.url.shortner.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.url.shortner.util.JsonDateDeserializer;
import org.url.shortner.util.JsonDateSerializer;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "url")
public class TinyUrl implements Serializable{
	private static final long serialVersionUID = 2436181457202378855L;

	@Id
	@Column(name="short_url",nullable=false)
	private String hash;
	
	@Column(name="original_url",nullable=false)
	private String originalUrl;

	@JsonSerialize(using=JsonDateSerializer.class)
	@JsonDeserialize(using=JsonDateDeserializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy",locale="en_IN")
	@Temporal(TemporalType.DATE)
	@Column(name ="creation_date",nullable = false, updatable = false)
    private Date creationDate;
    
	@JsonSerialize(using=JsonDateSerializer.class)
	@JsonDeserialize(using=JsonDateDeserializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy",locale="en_IN")
    @Temporal(TemporalType.DATE)
    @Column(name ="expiration_date",nullable = false, updatable = false)
    private Date expirationDate;


	public String getHash() {
		return hash;
	}


	public void setHash(String hash) {
		this.hash = hash;
	}


	public String getOriginalUrl() {
		return originalUrl;
	}


	public void setOriginalUrl(String originalUrl) {
		this.originalUrl = originalUrl;
	}


	public Date getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	public Date getExpirationDate() {
		return expirationDate;
	}


	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
}
