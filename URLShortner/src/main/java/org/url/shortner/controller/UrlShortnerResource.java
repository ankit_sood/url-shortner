package org.url.shortner.controller;

import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.url.shortner.exception.UrlNotFoundException;
import org.url.shortner.model.TinyUrl;
import org.url.shortner.repository.TinyUrlRepository;

import com.google.common.hash.Hashing;

@RestController
@RequestMapping("/rest")
public class UrlShortnerResource {
	@Autowired
	TinyUrlRepository tinyUrlRepository;
	
	Map<String,TinyUrl> urlMap = new HashMap<>();
	
	@GetMapping("/{id}")
	public String getUrl(@PathVariable("id") String url) throws Exception {
		TinyUrl tinyUrl = tinyUrlRepository.findById(url).orElseThrow(() -> new UrlNotFoundException(url));
		//check if url has expired or not
		return tinyUrl.getOriginalUrl();
	}
	
	@PostMapping("/url")
	public String createUrl(@RequestBody TinyUrl tinyUrl) throws MalformedURLException {
		UrlValidator urlValidator = new UrlValidator(new String[] {"http","https"});
		if(urlValidator.isValid(tinyUrl.getOriginalUrl())) {
			String urlId = Hashing.murmur3_32().hashString(tinyUrl.getOriginalUrl(), StandardCharsets.UTF_8).toString();
			tinyUrl.setHash(urlId);
			tinyUrl.setCreationDate(new Date());
			tinyUrlRepository.save(tinyUrl);
			urlMap.put(urlId, tinyUrl);
			return urlId;
		}
		return "Invalid URL";
	}
	
	@DeleteMapping("/{id}")
	public void deleteUrl(@PathVariable("id") String url) throws Exception {
		tinyUrlRepository.deleteById(url);
	}
}
