package org.url.shortner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.url.shortner.model.TinyUrl;

@Repository
public interface TinyUrlRepository extends JpaRepository<TinyUrl, String>{

}
