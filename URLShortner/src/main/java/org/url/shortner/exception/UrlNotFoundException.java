package org.url.shortner.exception;

public class UrlNotFoundException extends RuntimeException{
	private static final long serialVersionUID = -8600082261773689672L;
	
	public UrlNotFoundException(String url){
		super("Not able to find the mapping for: "+url);
	}
}
